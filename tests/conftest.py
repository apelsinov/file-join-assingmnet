import os
import pytest
import json
import csv


@pytest.fixture(scope="session")
def create_file():
    try:
        __import__(os.getenv("TESTFILE"))
    except ModuleNotFoundError as e:
        raise AssertionError(f"Файл должен быть загружен под именем {os.getenv('TESTFILE')}.py")


@pytest.fixture(scope="session")
def result_file_data():
    with open("result.json") as f:
        return json.load(f)


@pytest.fixture(scope="session")
def data_amount():
    with open("books.csv") as books:
        books_data = list(csv.DictReader(books))

    with open("users.json") as users:
        users_data = json.load(users)

    return {
        "users_amount": len(users_data),
        "books_amount": len(list(books_data))
    }

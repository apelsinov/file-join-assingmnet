import os
import json
import random
import cerberus

validator = cerberus.Validator()


def test_file_creation(create_file):
    assert os.path.isfile("result.json"), \
        "Не создан файл result.json"


def test_file_valid_json():
    try:
        with open("result.json") as f:
            json.load(f)
    except json.decoder.JSONDecodeError:
        raise AssertionError("Структура файла не соответствует json нотации")


def test_users_amount(result_file_data, data_amount):
    assert len(result_file_data) == data_amount["users_amount"], \
        f"Количество пользователей должно соответствовать исходному: {data_amount['users_amount']}"


def test_books_amount(result_file_data, data_amount):
    books_given = 0

    for user in result_file_data:
        books_given += len(user["books"])

    assert books_given == data_amount["books_amount"], \
        f"Количество книг должно соответствовать исходному количеству в файле: {data_amount['books_amount']}"


def test_user_data_structure(result_file_data):
    schema = {
        "name": {"type": "string"},
        "gender": {"type": "string"},
        "address": {"type": "string"},
        "age": {"type": "number"},
        "books": {"type": "list"}
    }

    random_user = random.choice(result_file_data)

    assert validator.validate(random_user, schema), \
        "Элементы итоговой структуры не соответствуют заданию"


def test_book_structure(result_file_data):
    schema = {
        "title": {"type": "string"},
        "author": {"type": "string"},
        "pages": {"type": "number"},
        "genre": {"type": "string"},
    }

    random_book = random.choice(random.choice(result_file_data)["books"])
    assert validator.validate(random_book, schema), \
        "Структура книги не соотвествует заданию"

FROM python:3.8-alpine

WORKDIR /app

COPY requirements.txt .

RUN pip install -U pip && pip install -r requirements.txt

COPY . .

ADD https://raw.githubusercontent.com/konflic/front_example/master/data/users.json users.json
ADD https://raw.githubusercontent.com/konflic/front_example/master/data/books.csv books.csv

ENV TESTFILE=reference

# Тут нужно придумать как автоматизировать проверку кодстайла
RUN pylint ${TESTFILE}.py

CMD TESTFILE=${TESTFILE} PYTHONPATH=$(pwd) pytest --exitfirst
